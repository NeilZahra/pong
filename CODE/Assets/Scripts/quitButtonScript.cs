﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class quitButtonScript : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(() => quitGame());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void quitGame()
    {
        Application.Quit();
    }
}
