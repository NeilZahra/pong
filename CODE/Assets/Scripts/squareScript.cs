﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class squareScript : MonoBehaviour
{

    private float speed, xdirection, ydirection;

    // Use this for initialization
    void Start()
    {
        speed = Random.Range(5f, 15f);
        xdirection = Random.Range(-2f, 2f);
        ydirection = Random.Range(-2f, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
        transform.Translate(new Vector3(xdirection, ydirection) * speed * Time.deltaTime);
    }
}
