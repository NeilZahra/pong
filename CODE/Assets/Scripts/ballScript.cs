﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ballScript : MonoBehaviour
{
    GameObject ball;

    int randNum;

    //Text p1scoreText;
    //public int p1score = 0;

    //Text p2scoreText;
    //public int p2score = 0;

    void Start()
    {
        ball = GameObject.Find("ball");
        randNum = Random.Range(1, 3);
    }
    

    // Update is called once per frame
    void Update()
    {
        //p1scoreText.text = "Score:" + p1score;
        //p2scoreText.text = "Score:" + p2score;

        if (randNum == 1)
        {
            ball.GetComponent<Rigidbody2D>().velocity = new Vector3(3f, 15f);
        }
        else
        {
            ball.GetComponent<Rigidbody2D>().velocity = new Vector3(15f, 3f);
        }

    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "R")
    //    {
    //        p1score += 1;

    //        if(p1score == 5)
    //        {
    //            SceneManager.LoadScene("Level 2");
    //        }
    //    }

    //    if (collision.gameObject.tag == "L")
    //    {
    //        p2score += 1;

    //        if(p2score == 5)
    //        {
    //            SceneManager.LoadScene("Level 2");
    //        }
    //    }
    //}

}
