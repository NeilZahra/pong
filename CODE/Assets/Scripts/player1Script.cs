﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class player1Script : MonoBehaviour {
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePosition = Input.mousePosition;

        Vector3 paddlePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //screen edges(to keep the paddle from going out of the screen)
        float screenEdge = Camera.main.orthographicSize * Camera.main.aspect;

        //only keep the Y position of the mouse and keep the X position as -8
        transform.position = new Vector3(-8f, paddlePosition.y);
    }
}
